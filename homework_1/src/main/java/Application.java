import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik black = new Kotik("black", 3, "gav");
        Kotik white = new Kotik();

        white.setKotik("white", 5, "meow");
        black.liveAnotherDay();

        System.out.println(black.getName());
        System.out.println(black.getWeight());

        if (white.getMeow().equals(black.getMeow())) {
            System.out.println("Котики одинаково разговаривают");
        } else {
            System.out.println("Котики не одинаково разговаривают");
        }
        System.out.println("Количество котов: " + Kotik.getCount());
    }
}