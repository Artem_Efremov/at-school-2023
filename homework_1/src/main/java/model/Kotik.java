package model;

public class Kotik {
    private String name;
    private int weight;
    private String meow;
    private int satiety;
    private static int count;

    public Kotik(String name, int weight, String meow) {
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        this.satiety = 5;
        count++;
    }

    public Kotik() {
        this("Безымянный", 0, "Мяу");
    }

    public void setKotik(String name, int weight, String meow) {
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public static int getCount() {
        return count;
    }

    public boolean play() {
        return doAction("играет.");
    }

    public boolean sleep() {
        return doAction("гонится за мышью.");
    }

    public boolean chaseMouse() {
        return doAction("гонится за мышью.");
    }

    public boolean pawMassage() {
        return doAction("Массаж от котика.");
    }

    public boolean fastRun() {
        return doAction("Котик быстро побежал.");
    }

    public void liveAnotherDay() {
        for (int i = 0; i < 24; i++) {
            boolean resRandAction = randomAction();
            if (!resRandAction) {
                eat(5);
            }
        }
    }

    public void eat(int satiety) {
        this.satiety += satiety;
    }

    public void eat(int satiety, String foodName) {
        eat(satiety);
        System.out.println(name + " поел " + foodName);
    }

    public void eat() {
        eat(5, "sushi.");
    }

    private boolean doAction(String action) {
        if (!isCanActionAndEat()) {
            return false;
        }
        System.out.println(name + " " + action);
        satiety--;
        return true;
    }

    private boolean isCanActionAndEat() {
        if (satiety <= 0) {
            askEat();
            return false;
        }
        return true;
    }

    private void askEat() {
        System.out.println(name + " хочу есть.");
    }

    private boolean randomAction() {
        int actionId = (int) (Math.random() * 5) + 1;
        return switch (actionId) {
            case 1 -> sleep();
            case 2 -> pawMassage();
            case 3 -> fastRun();
            case 4 -> play();
            case 5 -> chaseMouse();
            default -> false;
        };
    }
}
